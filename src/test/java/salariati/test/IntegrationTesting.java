package salariati.test;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import salariati.controller.EmployeeController;
import salariati.enumeration.DidacticFunction;
import salariati.model.Employee;
import salariati.repository.interfaces.EmployeeRepositoryInterface;
import salariati.repository.mock.EmployeeMock;
import salariati.validator.EmployeeValidator;

import static junit.framework.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

public class IntegrationTesting {
    private EmployeeRepositoryInterface employeeRepository;
    private EmployeeController controller;
    private EmployeeValidator employeeValidator;

    @Before
    public void setUp() {
        employeeRepository = new EmployeeMock();
        controller         = new EmployeeController(employeeRepository);
        employeeValidator  = new EmployeeValidator();
    }
    @Test
    public void testAddNewEmployee() {
        Employee newEmployee = new Employee("ValidLastName", "1910509055057", DidacticFunction.ASISTENT, "3000","ValidFirstName");
        assertTrue(employeeValidator.isValid(newEmployee));
        controller.addEmployee(newEmployee);
        assertEquals(7, controller.getEmployeesList().size());
    }
    @Test
    public void modifyFunctionNonExisingEmployeeData(){
        Employee Ionel   = new Employee("Pacuraruu", "1234567890876", DidacticFunction.ASISTENT, "2500","Pacuraru");
        Employee Ionel2   = new Employee("Pacuraru", "1234567890876", DidacticFunction.CONFERENTIAR, "2500","Pacuraru");
        assertNull(employeeRepository.modifyEmployeeFunction(Ionel,DidacticFunction.CONFERENTIAR));
    }
    @Test
    public void  showList(){
        Assert.assertTrue(controller.getEmployeesList().size()>0);
        Assert.assertTrue(controller.getEmployeesList().get(0).getLastName().equals("Pacuraru"));
    }
    @Test
    public void BigBang(){
        int size=controller.getEmployeesList().size();
        Employee newEmployee = new Employee("ValidLastName", "1910509055057", DidacticFunction.ASISTENT, "3000","ValidFirstName");
        assertTrue(employeeValidator.isValid(newEmployee));
        controller.addEmployee(newEmployee);
        assertEquals(size+1, controller.getEmployeesList().size());
        Employee employee=new Employee(controller.getEmployeesList().get(1).getLastName(),controller.getEmployeesList().get(1).getCnp(),controller.getEmployeesList().get(1).getFunction(),controller.getEmployeesList().get(1).getSalary(),controller.getEmployeesList().get(1).getFirstName());
        employee.setFirstName("Test");
        employee.setLastName("Test");
        controller.modifyEmployee(controller.getEmployeesList().get(1),employee);
        Assert.assertEquals(controller.getEmployeesList().get(1).getFirstName(),"Pacuraru");

    }
    @Test
    public void TopDown(){
        Employee newEmployee = new Employee("ValidLastName", "1910509055057", DidacticFunction.ASISTENT, "3000","ValidFirstName");
        assertTrue(employeeValidator.isValid(newEmployee));
        controller.addEmployee(newEmployee);
        assertEquals(7, controller.getEmployeesList().size());
        Employee Ionel   = new Employee("Pacuraruu", "1234567890876", DidacticFunction.ASISTENT, "2500","Pacuraru");
        Employee Ionel2   = new Employee("Pacuraru", "1234567890876", DidacticFunction.CONFERENTIAR, "2500","Pacuraru");
        assertNull(employeeRepository.modifyEmployeeFunction(Ionel,DidacticFunction.CONFERENTIAR));
        Assert.assertTrue(controller.getEmployeesList().size()>0);
        Assert.assertEquals(controller.getEmployeesList().get(0).getLastName(),"ValidLastName");
    }
}
