package salariati.UI;

import com.sun.org.apache.xpath.internal.SourceTree;
import salariati.controller.EmployeeController;
import salariati.enumeration.DidacticFunction;
import salariati.model.Employee;
import salariati.validator.EmployeeValidator;

import java.util.List;
import java.util.Scanner;

public class Console
{
    EmployeeController employeeController;
    public Console(EmployeeController employeeController){
        this.employeeController=employeeController;
    }
    public void showMenu(){
        int choice;
        Scanner sc=new Scanner(System.in);
        do{
            System.out.println("Apasa 1 pentru adaugare unui angajat");
            System.out.println("Apasa 2 pentru modificarea unui angajat");
            System.out.println("Apasa 3 pentru afisarea angajatilor");
            System.out.println("Apasa 0 pentru exit");
            choice=sc.nextInt();
            if(choice==1)
            {
                addEmployee();
            }
            else if(choice==2){
                modificaFunctionEmployee();
            }
            else if(choice==3){
                afisareEmployee();
            }

        }while(choice!=0);
    }

    private void afisareEmployee() {
        List<Employee> employeeList=employeeController.getEmployeesList();
        for (Employee employee:employeeList){
            System.out.println(employee);
        }
    }

    private void modificaFunctionEmployee() {
        Employee employee=createEmployee();
        DidacticFunction function=DidacticFunction.ASISTENT;
        int optiune;
        Scanner sc=new Scanner(System.in);
        do{
            System.out.println("Functie: 1=ASISTENT, 2=LECTURER, 3=TEACHER, 4=CONFERENTIAR");
            optiune=sc.nextInt();
        }while(!(optiune>=1 && optiune<=4));
        switch (optiune) {
            case 1:
                function = DidacticFunction.ASISTENT;
                break;
            case 2:
                function = DidacticFunction.LECTURER;
                break;
            case 3:
                function = DidacticFunction.ASISTENT;
                break;
            case 4:
                function = DidacticFunction.CONFERENTIAR;

        }
        employeeController.modifyEmployeeFunction(employee,function);

    }


    private void addEmployee() {
        Employee employee=createEmployee();
        addEmployee1(employee.getLastName(),employee.getCnp(),employee.getFunction(),employee.getSalary(),employee.getFirstName());

    }

    private Employee createEmployee() {
        Scanner sc=new Scanner(System.in);
        String nume;
        do {
            System.out.println("Numele:");
            nume = sc.next();
        }while (!(nume.length()>2 && nume.length()<25));
        String firstName;
        do {
            System.out.println("Prenume:");
            firstName = sc.next();
        }while (!(nume.length()>2 && nume.length()<25));
        String cnp;
        do{
            System.out.println("Cnp:");
            cnp=sc.next();}
        while (cnp.length()!=13);
        int optiune;
        DidacticFunction function;
        do{
            System.out.println("Functie: 1=ASISTENT, 2=LECTURER, 3=TEACHER, 4=CONFERENTIAR");
            function=DidacticFunction.CONFERENTIAR;
            optiune=sc.nextInt();
        }while(!(optiune>=1 && optiune<=4));
        switch (optiune){
            case 1:
                function=DidacticFunction.ASISTENT;
                break;
            case 2:
                function=DidacticFunction.LECTURER;
                break;
            case 3:
                function=DidacticFunction.TEACHER;
                break;
            case 4:
                function=DidacticFunction.CONFERENTIAR;

        }
        String salariu;
        do{
            System.out.println("Salariu:");
            salariu=sc.next();
        }while(salariu.length()==0);
        return new Employee(nume,cnp,function,salariu,firstName);
    }

    private void addEmployee1(String nume,String cnp,DidacticFunction function,String salariu,String firstName){
       Employee employee=new Employee(nume,cnp,function,salariu,firstName);
        EmployeeValidator validator=new EmployeeValidator();
        if(validator.isValid(employee)){
            employeeController.addEmployee(employee);
        }
        else
        {
            System.out.println("Angajatul nu este valid");
        }
    }
}
