package salariati.repository.implementations;

import java.io.*;
import java.util.*;

import salariati.enumeration.DidacticFunction;
import salariati.exception.EmployeeException;

import salariati.model.Employee;

import salariati.repository.interfaces.EmployeeRepositoryInterface;
import salariati.validator.EmployeeValidator;

public class EmployeeImpl implements EmployeeRepositoryInterface {
	
	private final String employeeDBFile = "D:\\Facultate\\vvss\\vvss236part2\\vvss236\\02-ProiectSalariati\\ProiectSalariati\\src\\main\\java\\salariati\\employees.txt";
	private EmployeeValidator employeeValidator = new EmployeeValidator();

	@Override
	public boolean addEmployee(Employee employee) {
		if( employeeValidator.isValid(employee) ) {
			BufferedWriter bw = null;
			try {
				bw = new BufferedWriter(new FileWriter(employeeDBFile, true));
				bw.write(employee.toString());
				bw.newLine();
				bw.close();
				return true;
			} catch (IOException e) {
				System.out.println("Eroare nu s-a putut scrie in fisier");
			}
		}
		return false;
	}

	@Override
	public void deleteEmployee(Employee employee) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void modifyEmployee(Employee oldEmployee, Employee newEmployee) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<Employee> getEmployeeList() {
		List<Employee> employeeList = new ArrayList<Employee>();
		
		BufferedReader br = null;
		try {
			br = new BufferedReader(new FileReader(employeeDBFile));
			String line;
			int counter = 0;
			while ((line = br.readLine()) != null) {
				Employee employee = new Employee();
				try {
					employee = employee.getEmployeeFromString(line, counter);
					employeeList.add(employee);
				} catch(EmployeeException ex) {
					System.err.println("Error while reading: " + ex.toString());
				}
				counter++;
			}
		} catch (FileNotFoundException e) {
			System.err.println("Error while reading: " + e);
		} catch (IOException e) {
			System.err.println("Error while reading: " + e);
		} finally {
			if (br != null)
				try {
					br.close();
				} catch (IOException e) {
					System.err.println("Error while closing the file: " + e);
				}
		}
        Collections.sort(employeeList, new Comparator<Employee>() {
            @Override
            public int compare(Employee o1, Employee o2) {
                if(Integer.parseInt(o1.getSalary()) < Integer.parseInt(o2.getSalary())){
                    return 1;
                }
                else if(Integer.parseInt(o1.getSalary()) > Integer.parseInt(o2.getSalary())){
                    return -1;
                }
                else{
                    if(o1.getCnp().charAt(1)<o2.getCnp().charAt(1))
                        return 1;
                    else if(o1.getCnp().charAt(2)<o2.getCnp().charAt(2))
                        return 1;
                    else if(o1.getCnp().charAt(3)<o2.getCnp().charAt(3))
                        return 1;
                    else if(o1.getCnp().charAt(4)<o2.getCnp().charAt(4))
                        return 1;
                    else if(o1.getCnp().charAt(5)<o2.getCnp().charAt(5))
                        return 1;
                    else if(o1.getCnp().charAt(6)<o2.getCnp().charAt(6))
                        return 1;
                    return -1;
                }
            }
        });
		return employeeList;
	}

    @Override
    public List<Employee> getEmployeeByCriteria(String criteria) {
        List<Employee> employeeList = getEmployeeList();


        return employeeList;
    }



	@Override
	public Employee modifyEmployeeFunction(Employee employee, DidacticFunction didacticFunction) {
		List<Employee> employees=getEmployeeList();
		for (Employee employee1:employees
			 ) {
			if(employee1==employee1){
				employee1.setFunction(didacticFunction);
				return employee1;
			}

		}
		return null;
	}

}
