package salariati.test;

import org.junit.Before;
import org.junit.Test;
import salariati.controller.EmployeeController;
import salariati.enumeration.DidacticFunction;
import salariati.model.Employee;
import salariati.repository.interfaces.EmployeeRepositoryInterface;
import salariati.repository.mock.EmployeeMock;
import salariati.validator.EmployeeValidator;

import static org.junit.Assert.*;

public class ModifyEmployeeTest {

    private EmployeeRepositoryInterface employeeRepository;
    private EmployeeController controller;
    private EmployeeValidator employeeValidator;

    @Before
    public void setUp() {
        employeeRepository = new EmployeeMock();
        controller         = new EmployeeController(employeeRepository);
        employeeValidator  = new EmployeeValidator();
    }

    @Test
    public void modifyFunctionNonExisingEmployeeData(){
        Employee Ionel   = new Employee("Pacuraruu", "1234567890876", DidacticFunction.ASISTENT, "2500","Pacuraru");
        Employee Ionel2   = new Employee("Pacuraru", "1234567890876", DidacticFunction.CONFERENTIAR, "2500","Pacuraru");
        assertNull(employeeRepository.modifyEmployeeFunction(Ionel,DidacticFunction.CONFERENTIAR));
    }
    @Test
    public void modifyFunctionValidData(){
            Employee Ionel   = new Employee("Pacuraru", "1234567890876", DidacticFunction.ASISTENT, "2500","Pacuraru");
        Employee Ionel2   = new Employee("Pacuraru", "1234567890876", DidacticFunction.CONFERENTIAR, "2500","Pacuraru");
        assertEquals(employeeRepository.modifyEmployeeFunction(Ionel,DidacticFunction.CONFERENTIAR).getFunction(),Ionel2.getFunction());
    }
}
