package salariati.repository.mock;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import salariati.enumeration.DidacticFunction;

import salariati.model.Employee;
import salariati.repository.interfaces.EmployeeRepositoryInterface;
import salariati.validator.EmployeeValidator;

public class EmployeeMock implements EmployeeRepositoryInterface {

	private List<Employee> employeeList;
	private EmployeeValidator employeeValidator;
	
	public EmployeeMock() {
		
		employeeValidator = new EmployeeValidator();
		employeeList = new ArrayList<Employee>();
		
		Employee Ionel   = new Employee("Pacuraru", "1234567890876", DidacticFunction.ASISTENT, "2800","Pacuraru");
		Employee Mihai   = new Employee("Dumitrescu", "1234567890876", DidacticFunction.LECTURER, "2500","Pacuraru");
		Employee Ionela  = new Employee("Ionescu", "1234567890876", DidacticFunction.LECTURER, "2500","Pacuraru");
		Employee Mihaela = new Employee("Pacuraru", "1234567890876", DidacticFunction.ASISTENT, "2500","Pacuraru");
		Employee Vasile  = new Employee("Georgescu", "1234567890876", DidacticFunction.TEACHER,  "2500","Pacuraru");
		Employee Marin   = new Employee("Puscas", "1234567890876", DidacticFunction.TEACHER,  "2500","Pacuraru");
		
		employeeList.add( Ionel );
		employeeList.add( Mihai );
		employeeList.add( Ionela );
		employeeList.add( Mihaela );
		employeeList.add( Vasile );
		employeeList.add( Marin );
	}
	
	@Override
	public boolean addEmployee(Employee employee) {
		if ( employeeValidator.isValid(employee)) {
			employeeList.add(employee);
			return true;
		}
		return false;
	}
	
	@Override
	public void deleteEmployee(Employee employee) {
		// TODO Auto-generated method stub
	}

	@Override
	public void modifyEmployee(Employee oldEmployee, Employee newEmployee) {
		// TODO Auto-generated method stub
	}

	@Override
	public List<Employee> getEmployeeList() {
		Collections.sort(employeeList, new Comparator<Employee>() {
			@Override
			public int compare(Employee o1, Employee o2) {
				if(Integer.parseInt(o1.getSalary()) < Integer.parseInt(o2.getSalary())){
					return 1;
				}
				else if(Integer.parseInt(o1.getSalary()) > Integer.parseInt(o2.getSalary())){
					return -1;
				}
				else{
					if(o1.getCnp().charAt(1)<o2.getCnp().charAt(1))
						return 1;
					else if(o1.getCnp().charAt(2)<o2.getCnp().charAt(2))
						return 1;
					else if(o1.getCnp().charAt(3)<o2.getCnp().charAt(3))
						return 1;
					else if(o1.getCnp().charAt(4)<o2.getCnp().charAt(4))
						return 1;
					else if(o1.getCnp().charAt(5)<o2.getCnp().charAt(5))
						return 1;
					else if(o1.getCnp().charAt(6)<o2.getCnp().charAt(6))
						return 1;
					return -1;
				}
			}
		});
		return employeeList;
	}

	@Override
	public List<Employee> getEmployeeByCriteria(String criteria) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Employee modifyEmployeeFunction(Employee employee, DidacticFunction didacticFunction) {
		List<Employee> employees=getEmployeeList();
		for (Employee employee1:employees
				) {
			if(employee1.equals(employee)){
				employee1.setFunction(didacticFunction);
				return employee1;
			}

		}
		return null;
	}

}
