package salariati.test;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import salariati.controller.EmployeeController;
import salariati.repository.interfaces.EmployeeRepositoryInterface;
import salariati.repository.mock.EmployeeMock;
import salariati.validator.EmployeeValidator;

public class ListOfEmployeeShow {

    private EmployeeRepositoryInterface employeeRepository;
    private EmployeeController controller;
    private EmployeeValidator employeeValidator;

    @Before
    public void setUp() {
        employeeRepository = new EmployeeMock();
        controller         = new EmployeeController(employeeRepository);
        employeeValidator  = new EmployeeValidator();

    }

    @Test
    public void  showList(){
        Assert.assertTrue(controller.getEmployeesList().size()>0);
        Assert.assertTrue(controller.getEmployeesList().get(0).getLastName().equals("Pacuraru"));


    }
}
